import {
  Component,
  OnInit,Input
} from '@angular/core';
import {
  BodyService
} from '../body.service';
import {
  CoreDataService
} from '../core-data.service';
import {
  HttpClient
} from '@angular/common/http';
import * as $ from 'jquery';
import {
  NgbModal,NgbActiveModal
} from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { StopLossComponent } from "../stop-loss/stop-loss.component";
import { Subscription } from 'rxjs';
@Component({
  selector: 'ngbd-modal-content',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Hi there!</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p>Hello, {{name}}!</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Close</button>
    </div>
  `
})
export class NgbdModalContent {
  @Input() name;

  constructor(public activeModal: NgbActiveModal) {}
}
@Component({
  selector: 'app-deposit-funds',
  templateUrl: './deposit-funds.component.html',
  styleUrls: ['./deposit-funds.component.css']
})
export class DepositFundsComponent implements OnInit {

  invoiceList: any;
  public transactionid: any;
  public MSID: any;
  public currencyBalance:any;
  public usdbalance:any;
  public balencelist;
  private paymentorder: Subscription;
  private getappsettingApi: Subscription;
  private Invoicelistapi: Subscription;
  private adminbankdtlapi: Subscription;
  private bankname: any;
  public banks = [];
  totalCount;
  orderNo;
  noOfItemPerPage = '20';
  CURRENCYNAME;
  depositDisclaimer;
  loadMaxValue;
  loadMinValue;
  invoiceOrderNo;
  invoiceAmount;
  currencyId:any;
  aedbalance:any;
  CurrencyCode:any;
  currencyValue:any;
  public bankID:any;
  public Bankdetails: any;
  public Baddress: any;
  public maxvalue;
public minvalue;
public maxvalueAed;
public minvalueAed;
public loader:boolean=true;
  constructor(private modalService: NgbModal,private router: Router, private route: ActivatedRoute, public main: BodyService, public data: CoreDataService, private http: HttpClient,private _StopLossComponent:StopLossComponent) {
    this.main.getUserTransaction();
  }
  //fiatBalanceText: string;

 
public environmentSettingListObj;
  ngOnInit() {
    this.paymentOrderList(1);
    this.getAdminBankDetails();
    this.appsettingscall();
    this.currencyBalance =this.main.balencelist;
    this.usdbalance = localStorage.getItem('usdbalance');
    if(this.currencyBalance!=null){
      for(var i=0;i<this.currencyBalance.length;i++){
        if(this.currencyBalance[i].currencyCode=="USD"){
          this.usdbalance=this.currencyBalance[i].closingBalance;
          this.CurrencyCode = this.currencyBalance[i].currencyCode;
        }
        if(this.currencyBalance[i].currencyCode=="AED"){
          this.aedbalance = this.currencyBalance[i].closingBalance;
          this.CurrencyCode = this.currencyBalance[i].currencyCode;
        }
      }
    }

    this.CURRENCYNAME = this.data.CURRENCYNAME;

    var environmentSettingListObj = JSON.parse(localStorage.getItem('environment_settings_list'));
  }

  appsettingscall() {
    var infoObj = {};
    infoObj['userId'] = localStorage.getItem('user_id');
    var jsonString = JSON.stringify(infoObj);
    this.getappsettingApi=this.http.post<any>(this.data.WEBSERVICE + '/user/GetUserAppSettings', jsonString, {
      headers: {
        'Content-Type': 'application/json',
        'authorization': 'BEARER ' + localStorage.getItem('access_token'),
      }
    })
      .subscribe(response => {
        var result = response;
        if (result.error.error_data != '0') {
        } else {
          var storeDashboardInfo = JSON.stringify(result);
          var environmentSettingsListObj: any = {};
          localStorage.setItem('user_app_settings_list', JSON.stringify(result.userAppSettingsResult));
          for (var i = 0; i < result.settingsList.length; i++) {
            environmentSettingsListObj['' + result.settingsList[i].name + result.settingsList[i].currencyId + ''] = result.settingsList[i];
          }
          environmentSettingsListObj = JSON.stringify(environmentSettingsListObj);
          localStorage.setItem('environment_settings_list', environmentSettingsListObj);
          this.environmentSettingListObj = JSON.parse(localStorage.getItem('environment_settings_list'));
          if(this.environmentSettingListObj["load_max_value1"].currencyId=='1'){
            this.depositDisclaimer = this.environmentSettingListObj["load_max_value1"].description;
            this.loadMaxValue =this.environmentSettingListObj["load_max_value1"].value;
            this.loadMinValue = this.environmentSettingListObj["load_min_value1"].value;

            var Tiretype= localStorage.getItem('UserTiretype');
       
            for (var i = 0; i < result.tierWiseTransactionSettingsList.length; i++) {
              if(result.tierWiseTransactionSettingsList[i].currencyId=='1'&& result.tierWiseTransactionSettingsList[i].tierType==Tiretype){
               this.maxvalue=result.tierWiseTransactionSettingsList[i].dailySendLimit;
               this.minvalue=result.tierWiseTransactionSettingsList[i].minLimit;
              
              }
              if(result.tierWiseTransactionSettingsList[i].currencyId=='28'&& result.tierWiseTransactionSettingsList[i].tierType==Tiretype){
               this.maxvalueAed=result.tierWiseTransactionSettingsList[i].dailySendLimit;
               this.minvalueAed=result.tierWiseTransactionSettingsList[i].minLimit;
              
              }
          
           }
          }
        }
      }, reason => {
        this.data.logout();
        if (reason.error.error == 'invalid_token') {
          this.data.alert('Session Timeout. Login Again', 'warning');
        } else this.data.alert('Could Not Connect To Server', 'danger');
      });
  }

  deposit_amount;
  orderNoToShow;

  
  depositAmount(content) {
    
    if (this.deposit_amount != undefined || this.bankID=="Select Bank Name") {
      if(this.bankname!=undefined ||this.bankID=="Select Bank Name"){
        if(this.bankname!=""){
      if (this.main.userBankVerificationStatus()) {
      if(this.CurrencyCode=='USD'){
        if (parseFloat(this.deposit_amount) <= parseFloat(this.maxvalue) && parseFloat(this.deposit_amount) >= parseFloat(this.minvalue)) {
          this.loader=true;
          var depositObj = {};
          depositObj['sendAmount'] = this.deposit_amount.toFixed(2);
          depositObj['currencyId'] = this.currencyId;
          depositObj['userId'] = localStorage.getItem('user_id');
          depositObj['bankId'] = this.bankID;
          var jsonString = JSON.stringify(depositObj);
          this.paymentorder = this.http.post<any>(this.data.WEBSERVICE + '/transaction/createPaymentOrder', jsonString, {
            headers: {
              'Content-Type': 'application/json',
              'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            }
          })
            .subscribe(response => {
              var result = response;
              if (result.error.error_data != '0') {
                this.deposit_amount = '';
                this.data.alert(result.error.error_msg, 'warning');
              } else {
                this.loader=false;
                this.modalService.open(content, {
                  centered: true
                });
                this.deposit_amount = '';
                this.paymentOrderList(1);
                this.main.getUserTransaction();
                this.getAdminBankDetails();
                this.orderNoToShow = result.paymentOrdersListResult[0].order_no;
                this.bankname=this.bankname;
                
              }
            }, function (reason) {
              if (reason.data.error == 'invalid_token') {
                this.data.logout();
              } else {
                this.data.logout();
                this.data.alert('Could Not Connect To Server', 'danger');
              }
            });

        } else {
          this.data.alert(this.depositDisclaimer, 'warning');
          this.loader=false;
        }
      }
      else{
        if (parseFloat(this.deposit_amount) <= parseFloat(this.maxvalueAed) && parseFloat(this.deposit_amount) >= parseFloat(this.minvalueAed)) {
          this.loader=true;
          var depositObj = {};
          depositObj['sendAmount'] = this.deposit_amount.toFixed(2);
          depositObj['currencyId'] = this.currencyId;
          depositObj['userId'] = localStorage.getItem('user_id');
          depositObj['bankId'] = this.bankID;
          var jsonString = JSON.stringify(depositObj);
          this.paymentorder = this.http.post<any>(this.data.WEBSERVICE + '/transaction/createPaymentOrder', jsonString, {
            headers: {
              'Content-Type': 'application/json',
              'authorization': 'BEARER ' + localStorage.getItem('access_token'),
            }
          })
            .subscribe(response => {
              var result = response;
              if (result.error.error_data != '0') {
                this.deposit_amount = '';
                this.data.alert(result.error.error_msg, 'warning');
              } else {
                this.loader=false;
                this.modalService.open(content, {
                  centered: true
                });
                this.deposit_amount = '';
                this.paymentOrderList(1);
                this.main.getUserTransaction();
                this.getAdminBankDetails();
                this.orderNoToShow = result.paymentOrdersListResult[0].order_no;
                this.bankname=this.bankname;
                
              }
            }, function (reason) {
              if (reason.data.error == 'invalid_token') {
                this.data.logout();
              } else {
                this.data.logout();
                this.data.alert('Could Not Connect To Server', 'danger');
              }
            });

        } else {
          this.data.alert(this.depositDisclaimer, 'warning');
          this.loader=false;
        }

      }
      } else {
        $('#payn').attr('disabled', true);
        this.data.alert('Your Bank Details are not being verified', 'warning');
      }
    }
    else{

      this.data.alert('Please select Bank Name', 'warning');
    }
    }
    else{
      this.data.alert('Please select Bank Name', 'warning');
    }
    } else {
      this.data.alert('Please Provide Deposit Amount', 'info');
    }
   
  }
claearbankdetails(){
  this.modalService.dismissAll();
  this.accountNo = "";
  this.routingNo ="";
  this.routingNo="";
  this.swiftCode="";
  this.beneficiaryName="";
  this.bankname="";
  this.ibanNo="";
  
}

  paymentOrderList(pageNo) {
    var paymentOrderObj = {};
    paymentOrderObj['userId'] = localStorage.getItem('user_id');
    paymentOrderObj['pageNo'] = pageNo.toString();
    paymentOrderObj['noOfItemsPerPage'] = this.noOfItemPerPage;
    var jsonString = JSON.stringify(paymentOrderObj);
   this.Invoicelistapi= this.http.post<any>(this.data.WEBSERVICE + '/transaction/getInvoicesList', jsonString, {
      headers: {
        'Content-Type': 'application/json',
        'authorization': 'BEARER ' + localStorage.getItem('access_token'),
      }
    })
      .subscribe(response => {
        var result = response;
        if (result.error.error_data != '0') {
          // alert(result.error.error_msg);
        } else {
          var paymentOrdersListResult = result.invoicesListResult;
          var paymentOrderListHtml = '';
          this.totalCount = result.totalCount;
          $('.payment_order_list_body').html('');
          if (paymentOrdersListResult != null) {
            this.invoiceList = paymentOrdersListResult;
            for (var i = 0; i < paymentOrdersListResult.length; i++) {
              var timestamp = paymentOrdersListResult[i].created;
              var timestampArr = timestamp.split('.');
              timestamp = timestampArr[0];
              if (paymentOrdersListResult[i].paymentOrders.status == '15') {
                var status = 'Order Received';
                var actionText = 'View Invoice';
                var actionTextColor = 'text-blue';
              }
              if (paymentOrdersListResult[i].paymentOrders.status == '16') {
                var status = 'Reference Updated';
                var actionText = 'Update Reference Number';
                var actionTextColor = 'text-yellow';
              }
              if (paymentOrdersListResult[i].paymentOrders.status == '17') {
                var status = 'Order Confirm';
                var actionText = 'Order Confirm';
                var actionTextColor = 'text-green';
              }
              this.orderNo = paymentOrdersListResult[i].paymentOrders.order_no;
              paymentOrderListHtml += '<tr>';
              paymentOrderListHtml += '<td>' + timestamp + '</td>';
              paymentOrderListHtml += '<td>' + this.orderNo + '</td>';
              paymentOrderListHtml += '<td class="text-white">' + ' ' + (paymentOrdersListResult[i].fiat_amount).toFixed(4) + '</td>';
              paymentOrderListHtml += '<td><span class="text-blue" data-order-id="' + this.orderNo + '" data-invoice-id="' + paymentOrdersListResult[i].invoice_id + '" onclick="angular.element(this).scope().getInvoiceDetails(this)" style="cursor:pointer;">View Invoice</span></td>';
              paymentOrderListHtml += '</tr>';
            }
            this.main.pagination(this.totalCount, this.noOfItemPerPage, 'paymentOrderList');
          } else {
            paymentOrderListHtml += '<tr colspan="5">No Data Exist</tr>';
          }
        }

      }, function (reason) {
        if (reason.data.error == 'invalid_token') {
          this.data.logout();
        } else {
          this.data.logout();
          this.data.alert('Could Not Connect To Server', 'danger');
        }
      });
  }

  bankName;
  routingNo;
  ibanNo;
  accountNo;
  bankUserName;
  bankAddress;
  currentbalence;
  currencyname;
  swiftCode;
  beneficiaryName;
  dep_amount;
  disclaim: boolean;
  password;
  TransactionId;
  fname;
  lname;
  Name;
  email;
  IbanNo;
  
  getAdminBankDetails() {
    var bankDetailsObj = {};
    bankDetailsObj['userId'] = localStorage.getItem('user_id');
    var jsonString = JSON.stringify(bankDetailsObj);
    this.adminbankdtlapi = this.http.post<any>(this.data.WEBSERVICE + '/user/GetAdminBankDetails', jsonString, {
      headers: {
        'Content-Type': 'application/json',
        'authorization': 'BEARER ' + localStorage.getItem('access_token'),
      }
    })
      .subscribe(response => {
        var result = response;
        if (result.error != '') {
          this.data.alert('Eroor', 'danger');
        } else {
          var bankdetl = response.adminBankDetails;
          this.Bankdetails = response.adminBankDetails;
         // console.log(this.Bankdetails);
          var x = bankdetl.length;
          for (var i = 0; i <= x - 1; i++) {
            this.banks[i] = { 'bankname': bankdetl[i].bankName, "bankId": bankdetl[i].bankId };
          }
        }
      }, function (response) {
        
        //   wip(0);
        if (response.data.error == 'invalid_token') {
          this.data.logout();
        } else {
          this.data.logout();
          this.data.alert('Could Not Connect To Server', 'danger');
        }
      });

  }
  gebankdtl(event) {
    var bankID = event;
    var x = this.Bankdetails.length;
    for (var i = 0; i <= x; i++) {
      if (bankID == this.Bankdetails[i].bankId) {
        this.accountNo = this.Bankdetails[i].accountNo;
        this.routingNo = this.Bankdetails[i].routingNo;
        this.ibanNo = this.Bankdetails[i].ibanNo;
        this.Baddress = this.Bankdetails[i].address;
        this.bankname=this.Bankdetails[i].bankName;
        this.beneficiaryName=this.Bankdetails[i].beneficiaryName;
        this.swiftCode=this.Bankdetails[i].swiftCode;
        this.bankID=this.Bankdetails[i].bankId;
      }
      else if(bankID=="Select Bank Name"){
        this.bankname="";
        this.accountNo = "";
        this.routingNo ="";
        this.ibanNo ="";
        this.swiftCode="";
        this.beneficiaryName="";
        this.Baddress = "";
      }
    }

  }

  invoice(content, id,amount,bankname,accountNo,RoutingNo,bfnm,swiftCode,currencyCode,ibnNo) {
    
  //  console.log(ibnNo);
    this.modalService.open(content, {
      centered: true
    });
    this.invoiceOrderNo = id;
    this.invoiceAmount = amount;
    this.bankName=bankname;
    this.accountNo=accountNo;
    this.routingNo=RoutingNo;
    this.beneficiaryName = bfnm;
    this.swiftCode=swiftCode;
    this.currencyname = currencyCode;
    this.ibanNo = ibnNo;
  }

  myFunction() {
    var checkBox = document.getElementById("disclaim");
    var text = document.getElementById("text");
    this.TransactionId = localStorage.getItem('user_id') + Date.now();
    var checked = document.forms["uc-cart-checkout-form"]["disclaim"].checked;
    if (checked == true) {
      this.disclaim = true;
    } else {
      this.disclaim = false;
    }
  }

  Opendisclaimer(depodisc, CURRENCYNAME, balence) {
    this.modalService.open(depodisc, {
      centered: true
    });
    this.currencyname = CURRENCYNAME;
    this.currentbalence = balence;
    this.dep_amount = ((<HTMLInputElement>document.getElementById("deposit_amount")).value);
    this.Name = localStorage.getItem('user_name');
    var temp=this.Name.split(" ")
    this.fname=temp[0];
    this.lname=temp[1];
    if(this.lname=="undefined" || this.lname=="")
    this.lname = '';
    this.email = localStorage.getItem('email');
  }

  getDepositModal(md, elem, bal,cId){
    this.CurrencyCode = elem;
    this.currencyValue = bal;
   this.currencyId=cId;
   localStorage.setItem("currencyId",this.currencyId);
     this.modalService.open(md, {
       centered: true
     });
 }

  ngOnDestroy() {
    if (this.getappsettingApi != undefined) {
      this.getappsettingApi.unsubscribe();
    }
    if (this.paymentorder != undefined) {
      this.paymentorder.unsubscribe();
    }
    if (this.Invoicelistapi != undefined) {
      this.Invoicelistapi.unsubscribe();
    }
    if (this.Invoicelistapi != undefined) {
      this.Invoicelistapi.unsubscribe();
    }
  }
}