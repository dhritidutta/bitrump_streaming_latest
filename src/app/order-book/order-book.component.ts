import { Component, OnInit, DoCheck,OnDestroy } from "@angular/core";

import { TradesComponent } from "../trades/trades.component";

import * as $ from "jquery";

import { CoreDataService } from "../core-data.service";

import { Router } from "@angular/router";

import { DashboardComponent } from "../dashboard/dashboard.component";

import { HttpClient } from "@angular/common/http";

import { tokenKey } from "@angular/core/src/view";

import { ChartComponent } from "../../app/chart/chart.component";
import {StopLossComponent} from "../stop-loss/stop-loss.component"
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
declare const toggleslide: any;

export interface Todo {
  id: number | string;
  createdAt: number;
  value: string;
}

@Component({
  selector: "app-order-book",

  templateUrl: "./order-book.component.html",

  styleUrls: ["./order-book.component.css"]
})
export class OrderBookComponent implements OnInit {
  buyingAssetIssuer: string;

  sellingAssetIssuer: string;

  bidBody: string;

  askBody: string;

  orderBookUrl: string;

  count: any;

  lowprice: any;
  highprice;
  any = 0;
  chartlist: any = 0;

  ctpdata: any = 0;
  ltpdata: any = 0;
  act:any;
  rocdata:any;
  volumndata:any;
  marketTradeBodyHtml: any;
  public biddata:any;
  public askdata:any;
  public source1:any;
  urlBid: any;

  urlAsk: any;

  selectedBuyingCryptoCurrencyName: string;

  selectedSellingCryptoCurrencyName: string;

  sellingAssetType: string;

  buyingAssetType: string;
  source12: any;
  source:any;
  token: any;
  marketTradeRecords: any;
  itemcount: any;
  rocreact:any;
  private baseUrl = 'https://mainnet.bitrump.com/';
  //private dataStore: { todos: Todo[] } = { todos: [] };
  filterCurrency: any;
  header: any;
  assets: any;
  currency_code: any;
  base_currency: any;
  selectedBuyingAssetText: string;
  selectedSellingAssetText: string;
  assetpairbuy: string;
  assetpairsell: string;
  responseBuySell: any;
  assetcount=[];
  assetetpair=[];
    roccolor;
  constructor(
    public data: CoreDataService,
    private route: Router,
    public dash: DashboardComponent,
    private http: HttpClient,
    private trade:TradesComponent,
    private _StopLossComponent:StopLossComponent

  ) {
    switch (this.data.environment) {
      case "live":
        this.orderBookUrl = "https://mainnet.bitrump.com/"; //live
        break;

      case "dev":
        this.orderBookUrl =
          ""; //dev
        break;

      case "uat":
        this.orderBookUrl = ""
        //"http://ec2-52-8-41-112.us-west-1.compute.amazonaws.com:8000/"; //uat
        break;

      case "demo":
        this.orderBookUrl = "https://mainnet.bitrump.com/"; //demo
        break;
    }
   // $("#orderbookBidBody").html(this.bidBody);
  }


  ngOnInit() {

    this.selectedSellingCryptoCurrencyName = this.data.selectedSellingCryptoCurrencyName;
    this.selectedBuyingCryptoCurrencyName = this.data.selectedBuyingCryptoCurrencyName;
    this.currency_code = 'BTC';
    this.base_currency = 'USDT';
  
      this.serverSentEventForOrderbookAsk();
    ///  toggleslide()
      this.getNewCurrency('USDT');
    //  this.getMarketTradeBuy();

  }
  myFunction(){ 
  //  alert('mbnkb');
    document.getElementById("myDropdown").classList.toggle("show");

  }

  changemode() {
    // console.log('++++++++++++++',this.data.changescreencolor);
    if (this.data.changescreencolor == true) {
      $(".bg_new_class")
        .removeClass("bg-dark")
        .css("background-color", "#fefefe");
      $(".sp-highlow").css("background-color", "#d3dddd");
      $(".sp-highlow").css("color", "Black");
      $(".border-col").css("border-color", "#d3dddd");
      $("th").css({ "background-color": "#d3dddd", color: "#273338" });
      $(".text-left").css("color", "black");
      $(".text-right").css("color", "black");
    } else {
      $(".bg_new_class")
        .removeClass("bg-dark")
        .css("background-color", "#16181a");
      $(".sp-highlow").css("background-color", "#273338");
      $(".sp-highlow").css("color", "yellow");
      $(".border-col").css("border-color", "#273338");
      $("th").css({ "background-color": "#273338", color: "#d3dddd" });
      $(".text-left")
        .css("color", "")
        .css("color", "rgb(153, 152, 152)");
      $(".text-right").css("color", "rgb(153, 152, 152)");
    }
  }
  ngDoCheck() {
    this.changemode();
    var randomNoForFlashArr = [];

    var arraylength = 2;

    var valcurrency = this.data.ctpdata;

    if (valcurrency == undefined) {
      this.ctpdata = this.chartlist.CTP;
      this.ltpdata = this.chartlist.LTP;
      this.lowprice = this.chartlist.LOW_PRICE;
      this.highprice = this.chartlist.HIGH_PRICE;
      // console.log("data++++++++1", this.ctpdata, this.lowprice, this.highprice);
    } else {
      this.ctpdata = this.data.ctpdata;
      this.ltpdata = this.data.ltpdata;
      this.lowprice = this.data.lowprice;

      this.highprice = this.data.highprice;
      //console.log('data2++++++++',    this.ctpdata,this.lowprice, this.highprice)
    }
    var randomNo = this.randomNoForOrderBook(0, arraylength);
    randomNoForFlashArr.push(randomNo);
    for (var i = 0; i < arraylength; i++) {
      if (!$.inArray(i, randomNoForFlashArr)) {
        if (i == 0) {
          document.getElementById("pricered").style.display = "inline-block";

          document.getElementById("pricegreen").style.display = "none";
        } else {
          document.getElementById("pricered").style.display = "none";

          document.getElementById("pricegreen").style.display = "inline-block";
        }
      }
    }

  }

  getNewCurrency(currency) {
 // alert(currency);
 $("#asset-table .btn").click(function(){
   $("#asset-table .btn").removeClass('active');
    $(this).toggleClass('active');
 });
 
    this.assetetpair=[];
    this.http.get<any>('https://api.bitrump.com/CacheService/api/getData?Name=Assets')
      .subscribe(responseCurrency => {
        this.filterCurrency  = JSON.parse(responseCurrency.value);
       // var mainArr = [];
        this.header = this.filterCurrency.Header;
        this.assets = this.filterCurrency.Values;
     // console.log("PPPPPPP",this.assets);
        var basrcurrency=currency;
        var x;
        for(var i=0;i<=this.assets.length-1;i++){
        //  alert('change')
          if(basrcurrency==this.assets[i].baseCurrency){
          
            if(this.assets[i].roc>0){
              this.roccolor = true;
            }
            else{
              this.roccolor=false;
            //  this.data.negetive =false;
            }
            this.assetetpair.push({'currencycode':this.assets[i].currencyCode,'basecurrency':this.assets[i].baseCurrency,'lasttrade':this.assets[i].ltpValue,'roc':this.assets[i].roc,'volume':this.assets[i].volume,'RocColor':this.roccolor});

          //  for(var j = i << 1; j <= this.assets.length; j += i) {
          //   this.assetcount[j]=true;
          //  }
            
          }
         else if(basrcurrency=='ALL'){

            if(this.assets[i].baseCurrency=='AED'||this.assets[i].baseCurrency=='USD'){
              this.assetetpair.push({'currencycode':this.assets[i].currencyCode,'basecurrency':this.assets[i].baseCurrency,'lasttrade':this.assets[i].ltpValue,'roc':this.assets[i].roc,'volume':this.assets[i].volume});
              //this.assetetpair.push(this.assets[i].currencyCode+'/'+basrcurrency);
            }
          }

        }
       console.log('TTTTTTT',this.assetetpair);

      }
      )
  }
  // getassetIssuer(){
  //   this.http.get<any>('https://api.bitrump.com/CacheService/api/getData?Name=AssetIssuer')
  //     .subscribe(responseBuySell => {
  //        this.responseBuySell = JSON.parse( responseBuySell.value);

  //     })
  // }
  

  buySellCode(item) {
    //debugger;
  (item.currency=="BCC")?this.currency_code ="BCH":this.currency_code = item.currency; 
  document.getElementById("myDropdown").classList.toggle("show");
  //  var str=item.split("/");
  //  var arrayval=str;
  //  console.log(arrayval);
   // document.getElementById('asset-table').style.display='none'
    this.currency_code = item.currencycode;
    this.base_currency = item.basecurrency;
    this.data.selectedSellingAssetText = this.base_currency;
    this.data.selectedBuyingAssetText = this.currency_code;
    this.data.selectedBuyingCryptoCurrencyName =this.base_currency+ this.currency_code;
    this.data.selectedSellingCryptoCurrencyName = this.currency_code + this.base_currency;
    this.selectedBuyingAssetText = item.currencyCode;
    this.selectedSellingAssetText = item.baseCurrency;
    localStorage.setItem("buying_crypto_asset", this.currency_code.toLocaleLowerCase());
    localStorage.setItem("selling_crypto_asset", this.base_currency.toLocaleLowerCase());
    this.data.cur = this.selectedSellingAssetText;
    //localStorage.setItem("CurrencyID", this.currencyId);//change bu sanu
    this.assetpairsell = item.currencyCode + item.baseCurrency;
    this.assetpairbuy = item.baseCurrency + item.currencyCode;
    this.data.selectedSellingCryptoCurrencyissuer = "";
    this.data.selectedBuyingCryptoCurrencyissuer = "";
    // this.http.get<any>('https://api.bitrump.com/CacheService/api/getData?Name=AssetIssuer')
    //   .subscribe(responseBuySell => {
    //      this.responseBuySell = JSON.parse( responseBuySell.value);
     //   var x = this.responseBuySell.length;
        // for (var i = 0; i < x ; i++) {
        //   if (this.assetpairsell == this.responseBuySell[i].assetPair) {
        //     this.data.selectedSellingCryptoCurrencyissuer = this.responseBuySell[i].issuer;

        //   }
        //   else if (this.assetpairbuy == this.responseBuySell[i].assetPair) {
        //     this.data.selectedBuyingCryptoCurrencyissuer = this.responseBuySell[i].issuer;
        //   }
        //   else if (this.data.selectedSellingCryptoCurrencyissuer != "" && this.data.selectedBuyingCryptoCurrencyissuer != "") {
        //     break;
        //   }
        // }
        this.serverSentEventForOrderbookAsk();
       // this.getMarketTradeBuy();
        this.trade.reload();
      //  this._StopLossComponent.reset();
      $('.limit-reset').click();
       this.trade.myTradeDisplay(0);
       $('#trade').click();
        //  $('.get-offer').click();
             $('#dropHolder').css('overflow', 'scroll');
        $(window).resize(function () {
          var wd = $('#chartHolder').width();
          $('#dropHolder').width(wd);
          $('#dropHolder').css('overflow', 'scroll');
        });
      
       //var url='https://api.bitrump.com/StreamApi/rest/trendsTradeGraphFor24Hours/' + this.currency_code + '/' + this.base_currency;
       var url="https://api.bitrump.com/StreamingApi/rest/trendsTradeGraphFor24Hours/"+this.currency_code+"/"+this.base_currency;
        if (this.source1 != undefined ) {
          this.source1.close();
        }
        if (this.data.source3 != undefined ) {
          this.data.source3.close();
        }
      this.source1 = new EventSource(url);
      this.urlAsk = url;
      var result: any = new Object();
      this.source1.onmessage = (event: MessageEvent) => {
        result = event.data;
        result = JSON.parse(event.data);
        if (result.length !=0) {
          
          if(this.currency_code == 'BTC' ||  this.base_currency == 'USDT'){
            this.chartlist = result[0];
            this.ctpdata = this.data.ctpdata = this.chartlist.ctp;
            this.ltpdata = this.data.ltpdata = this.chartlist.ltp;
            this.lowprice = this.data.lowprice =parseFloat(this.chartlist.lowPrice).toFixed(2);
            this.highprice = this.data.highprice =parseFloat(this.chartlist.highPrice).toFixed(2);
            this.act = this.data.ACTION = this.chartlist.action;
            this.data.rocdata = parseFloat(this.chartlist.roc).toFixed(2);
            if (this.data.rocdata > 0) {
              this.rocreact = true;
            }
            else {
              this.data.rocreact = false;
            }
            if (this.data.rocdata < 0) {
              this.data.negetive = true;
            }
            else {
              this.data.negetive = false;
            }
            this.data.volumndata =  parseFloat(this.chartlist.volume).toFixed(2);
            if (this.data.rocdata >= 0) { this.rocreact = true }
            if (this.data.act == 'sell') {
              this.data.react = true;
            }
            else {
              this.data.react = false;
            }
          }
          else{
            this.chartlist = result[0];
            this.ctpdata = this.data.ctpdata = this.chartlist.ctp;
            this.ltpdata = this.data.ltpdata = this.chartlist.ltp;
            this.lowprice = this.data.lowprice = parseFloat(this.chartlist.lowPrice).toFixed(2);
            this.highprice = this.data.highprice = parseFloat(this.chartlist.highPrice).toFixed(2);
            this.act = this.data.ACTION = this.chartlist.action;
            this.data.rocdata = parseFloat(this.chartlist.roc).toFixed(2);
            if (this.data.rocdata > 0) {
              this.data.rocreact = true;
            }
            else {
              this.data.rocreact = false;
            }
            if (this.data.rocdata < 0) {
              this.data.negetive = true;
            }
            else {
              this.data.negetive = false;
            }
             this.data.volumndata =parseFloat(this.chartlist.volume).toFixed(2);
            if (this.data.rocdata >= 0) { this.rocreact = true }
            if (this.data.act == 'sell') {
              this.data.react = true;
            }
            else {
              this.data.react = false;
            }
          }
        }
        else{
          this.ctpdata=this.data.ctpdata = '0';
          this.ltpdata = this.data.ltpdata = '0';
              this.lowprice = this.data.lowprice= '0';
              this.highprice=this.data.highprice = '0';
              this.rocdata=this.data.rocdata ="";
              this.volumndata=this.data.volumndata = 0; 
        }
      
       } 
      // document.getElementById('asset-table').style.display='block'
     // })
  }
  //random function for flash class in server sent orderbook

  randomNoForOrderBook(minVal: any, maxVal: any): number {
    var minVal1: number = parseInt(minVal);

    var maxVal1: number = parseInt(maxVal);

    return Math.floor(Math.random() * (maxVal1 - minVal1 + 2) + minVal1);
  }

  getMarketTradeBuy() {
  
    $('#marketTradeBody').html('<tr><td colspan="3" class="text-center"><img src="./assets/svg-loaders/three-dots.svg" alt="" width="50"></td></tr>');
    var result: any;
    this.selectedBuyingAssetText = this.data.selectedBuyingAssetText;
    this.selectedSellingAssetText = this.data.selectedSellingAssetText;
    //this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer;
   // this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer;//'GBDWSH2SAZM3U5FR6LPO4E2OQZNVZXUAZVH5TWEJD64MGYJJ7NWF4PBX';
   // var url ='https://api.bitrump.com/StreamApi/rest/TradeHistory?baseAssetCode=' + this.data.selectedSellingCryptoCurrencyName.toUpperCase() + '&baseAssetIssuer=' + this.sellingAssetIssuer +'&counterAssetCode=' + this.data.selectedBuyingCryptoCurrencyName.toUpperCase() + '&counterAssetIssuer=' + this.buyingAssetIssuer + '&order=desc' + '&limit=' + 50;
    var url="https://api.bitrump.com/StreamingApi/rest/TradeHistory?baseAssetCode="+this.data.selectedSellingCryptoCurrencyName+"&counterAssetCode="+this.data.selectedBuyingCryptoCurrencyName+"&limit=50";    
    if (this.source != undefined ) {
      this.source.close();
    }
    this.source= new EventSource(url);
    this.source.addEventListener('message', event => {
      result = JSON.parse(event.data);
      this.marketTradeBodyHtml = '';
      if(result.response !=null){
      var response = JSON.parse(result.response);
      this.marketTradeRecords=response._embedded.records;
     // console.log('+++++++++ this.marketTradeRecords',this.marketTradeRecords);
     this.itemcount = this.marketTradeRecords.length;
      var arraylength = this.marketTradeRecords.length;
          if (arraylength != null) {
          this.marketTradeBodyHtml = '';
          var randomNoForFlashArrM = [];
          var randomNo = this.randomNoForOrderBook(0, 10);
          randomNoForFlashArrM.push(randomNo);
           for (var i = 0; i < this.marketTradeRecords.length; i++) {
            if (!$.inArray(i,randomNoForFlashArrM)) {
              if (randomNo % 2 == 0) {
                var pickclass='.'+i+'newclassmarket';
                 // alert('////'+randomNo+pickclass);
                // alert(pickclass);
                  $(pickclass).addClass('bg-flash-red');
   
                 } else if (randomNo % 2 != 0) {
                   var pickclass='.'+i+'newclassmarket';
                //   alert('////////'+randomNo+pickclass);
                  // var className = 'text-red';
                  $(pickclass).addClass("bg-flash-green");
                 }
       
             
            }
           
         
          }
      
        }
    
        else{
          //this.marketTradeBodyHtml += '<tr><td colspan="3" class="text-center text-danger">No Data</td></tr>';
        }
      }
      else{
       // this.marketTradeBodyHtml += '<tr><td colspan="3" class="text-center text-danger">No Data</td></tr>';
      }

      }, error => {
        //$('#marketTradeBody').html('<tr><td colspan="3" class="text-center text-danger">No Data Available</td></tr>');
      })

  }

 
  serverSentEventForOrderbookAsk() {
  //  $('#orderbookAskBody').html('<tr><td colspan="3" class="text-center"><img src="./assets/svg-loaders/three-dots.svg" alt="" width="50"></td></tr>');
    this.buyingAssetIssuer = this.data.selectedBuyingCryptoCurrencyissuer;
    this.sellingAssetIssuer = this.data.selectedSellingCryptoCurrencyissuer;
    this.token = localStorage.getItem('access_token');
    // alert(this.data.selectedSellingCryptoCurrencyName+"_____"+this.data.selectedBuyingCryptoCurrencyName)
    //var url = "https://api.bitrump.com/StreamApi/rest/OrderBook?sellingAssetCode=" + this.data.selectedSellingCryptoCurrencyName + "&sellingAssetIssuer=" + this.sellingAssetIssuer + "&buyingAssetCode=" + this.data.selectedBuyingCryptoCurrencyName+ "&buyingAssetIssuer=" + this.buyingAssetIssuer + "&limit=50";
   //var url= "https://api.bitrump.com/StreamingApi/rest/OrderBook?sellingAssetCode="+this.data.selectedSellingCryptoCurrencyName +"&buyingAssetCode="+this.data.selectedBuyingCryptoCurrencyName+"&limit=50"; 
   var url=" https://api.bitrump.com/StreamingApi/rest/depth/trades?sellingAssetCode="+this.data.selectedSellingCryptoCurrencyName+"&buyingAssetCode="+this.data.selectedBuyingCryptoCurrencyName+"&limit=50";
    if (this.data.source2 != undefined ) {
      this.data.source2.close();
    }
    if (this.token.length >= "null" || this.token.length >= 0) {
 
      this.data.source2 = new EventSource(url);
      this.urlAsk = url;
      var result: any = new Object();
      this.data.source2.onmessage = (event: MessageEvent) => {
        result = event.data;
     //   result = JSON.parse(event.data);
        var response = JSON.parse(event.data);
        var askdata = [];
        this.askdata = response.asks;
        var biddata = [];
       this.biddata = response.bids;
       var tradedata=response.trades;
       var response = JSON.parse(tradedata);
       this.marketTradeRecords=response._embedded.records;
       // console.log('++++++++++++++++++++++++++++++++++++++--------',tradedata);
      //  console.log('++++++++++++++++++++++++++++++++++++++--------', this.marketTradeRecords);
        var bidHtml = "";
        var askHtml = "";
        if (this.askdata.length != 0 ) {
          // if (askdata.length > 5) { //changed by sanu
          //   var askLength = askdata.length;
          // } else {
          //   var askLength = askdata.length;
          // }
          var randomNoForFlashArr1 = [];
          var randomNo = this.randomNoForOrderBook(0,this.askdata.length);
          randomNoForFlashArr1.push(randomNo);
          for (var i=0; i<10; i++) {
          //var className = "";
         
          if (!$.inArray(i,randomNoForFlashArr1)) {
            var pickclass1='.'+i+'newclass';

            $(pickclass1).addClass('bg-flash-red');

            }
         
           }
        } else {
          askHtml += "<tr>";
          askHtml += '<td class="text-white" colspan="2">No Data</td>';
          askHtml += "</tr>";
        }
    
        if (this.biddata.length != 0 ) {
          if (this.biddata.length > 5) { //changed by sanu
            var bidLength = this.biddata.length;
          } else {
            var bidLength = this.biddata.length;
          }
          var randomNoForFlashArr = [];
          var randomNo = this.randomNoForOrderBook(0,this.biddata.length);
          randomNoForFlashArr.push(randomNo);
        // console.log('+++++PPPPPPPPP',this.biddata);
        for (var i=0; i<10; i++) { //changed by sanu
     //   console.log('+++++PPPPPPPPP',biddata[i].amount);
      //  bidHtml=biddata[i].amount +" " +biddata[i].price;

         //    var className = "";
         if (!$.inArray(i, randomNoForFlashArr)) {

          var pickclass='.'+i+'newclassask';
         //alert('pp'+pickclass);
         $(pickclass).addClass('bg-flash-green');

          }
       
        
           }
        } else {
          bidHtml += "<tr>";
          bidHtml += '<td class="" colspan="2">No Data</td>';
          bidHtml += "</tr>";
        }
        this.bidBody = bidHtml;
       // console.log('+++++++++++++',this.bidBody);
       // $("#orderbookBidBody").html(this.bidBody);

      };
      // this.data.source2.error=function(err:any){
      //   console.error("EventSource failed:", err);
      // }
    } else {
      this.data.source2.close();
    }
  }
  ngOnDestroy(){
    if (this.data.source2 != undefined) {
      this.data.source2.close();
   
    }
    // if (this.source != undefined) {
     
    //   this.source.close();
    // }
    if (this.source1 != undefined) {
     
      this.source1.close();
   
    }
  }
 
}
