import { Component, OnInit, DoCheck } from "@angular/core";
import { BodyService } from "../body.service";
import { HttpClient } from "@angular/common/http";
import { CoreDataService } from "../core-data.service";
import { StopLossComponent } from "../stop-loss/stop-loss.component";
import * as $ from "jquery";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-withdraw-funds",
  templateUrl: "./withdraw-funds.component.html",
  styleUrls: ["./withdraw-funds.component.css"]
})
export class WithdrawFundsComponent implements OnInit {
  public currencyBalance: any;
  public usdbalance: any;
  constructor(
    public main: BodyService,
    private http: HttpClient,
    private modalService: NgbModal,
    public data: CoreDataService,
    public _StopLossComponent: StopLossComponent
  ) {
    this.main.getUserTransaction();
    this.main.getDashBoardInfo();
  }
  environmentSettingListObj;
  environmentSettingListObj1;
  withdrawMaxValue;
  withdrawMinValue;
  withdrawDisclaimer;
  withdrawTxnChargeDisclaimer;
  currencyId: any;
  CurrencyCode:any;
  aedbalance:any;
  currencyValue:any;
public maxvalue;
public minvalue;
public maxvalueAed;
public minvalueAed;
  ngOnInit() {
     this.getBankDetails();
   // this.main.getDashBoardInfo();
    this.appsettingscall();
    this.currencyBalance =this.main.balencelist;
    this.usdbalance = localStorage.getItem('usdbalance');
    if(this.currencyBalance!= null){
      for(var i=0;i<this.currencyBalance.length;i++){
        if(this.currencyBalance[i].currencyCode=="USD"){
          this.usdbalance=this.currencyBalance[i].closingBalance;
          this.CurrencyCode = this.currencyBalance[i].currencyCode;
          this.currencyId = this.currencyBalance[i].currencyId;
        }
        if (this.currencyBalance[i].currencyCode=="AED"){
          this.usdbalance=this.currencyBalance[i].closingBalance;
          this.CurrencyCode = this.currencyBalance[i].currencyCode;
          this.currencyId = this.currencyBalance[i].currencyId;
        }
      }
    }
    //  this.environmentSettingListObj=JSON.parse(localStorage.getItem('environment_settings_list'));
    // console.log(this.environmentSettingListObj);
    // alert(settingsList["withdrawal_max_value"]);
  //  this.withdrawMaxValue=this.environmentSettingListObj["withdrawal_max_value"+this.currencyId].value;
  //  this.withdrawMinValue=this.environmentSettingListObj["withdrawal_min_value"+this.currencyId].value;
  //   this.withdrawDisclaimer=this.environmentSettingListObj["withdrawal_min_value"+this.currencyId].description;
  //   this.withdrawTxnChargeDisclaimer=this.environmentSettingListObj["withdrawal_txn_charges"+this.currencyId].description;

  }
 
  appsettingscall() {
    
    var infoObj = {};
    infoObj['userId'] = localStorage.getItem('user_id');
    var jsonString = JSON.stringify(infoObj);
    this.http.post<any>(this.data.WEBSERVICE + '/user/GetUserAppSettings', jsonString, {
      headers: {
        'Content-Type': 'application/json',
        'authorization': 'BEARER ' + localStorage.getItem('access_token'),
      }
    })
      .subscribe(response => {
        var result = response;
      //  localStorage.setItem("currencyId",result.settingsList[i].currencyId);
     //   console.log('+++++++++++appsettings',response);
        if (result.error.error_data != '0') {
          //    alert(result.error.error_msg);
        } else {
          var storeDashboardInfo = JSON.stringify(result);
          var environmentSettingsListObj: any = {};
          localStorage.setItem('user_app_settings_list', JSON.stringify(result.userAppSettingsResult));
          for (var i = 0; i < result.settingsList.length; i++) {
            environmentSettingsListObj['' + result.settingsList[i].name + result.settingsList[i].currencyId + ''] = result.settingsList[i];
          }
          environmentSettingsListObj = JSON.stringify(environmentSettingsListObj);
          localStorage.setItem('environment_settings_list', environmentSettingsListObj);
          this.environmentSettingListObj = JSON.parse(localStorage.getItem('environment_settings_list'));
          if(this.environmentSettingListObj["withdrawal_min_value1"].currencyId=='1'){
            this.withdrawDisclaimer = this.environmentSettingListObj["withdrawal_min_value1"].description;
           this.withdrawTxnChargeDisclaimer = this.environmentSettingListObj["withdrawal_txn_charges1"].description;
           this.withdrawMaxValue =this.environmentSettingListObj["withdrawal_max_value1"].value;
           this.withdrawMinValue = this.environmentSettingListObj["withdrawal_min_value1"].value;
        //   var environmentSettingsListObj1: any = {};
        
        var Tiretype= localStorage.getItem('UserTiretype');
        localStorage.setItem('user_app_settings_list_tire', JSON.stringify(result.tierWiseTransactionSettingsList));
           for (var i = 0; i < result.tierWiseTransactionSettingsList.length; i++) {
             if(result.tierWiseTransactionSettingsList[i].currencyId=='1'&& result.tierWiseTransactionSettingsList[i].tierType==Tiretype){
              this.maxvalue=result.tierWiseTransactionSettingsList[i].dailySendLimit;
              this.minvalue=result.tierWiseTransactionSettingsList[i].minLimit;
             
             }
             if(result.tierWiseTransactionSettingsList[i].currencyId=='28'&& result.tierWiseTransactionSettingsList[i].tierType==Tiretype){
              this.maxvalueAed=result.tierWiseTransactionSettingsList[i].dailySendLimit;
              this.minvalueAed=result.tierWiseTransactionSettingsList[i].minLimit;
             
             }
          
          }
          

      //     environmentSettingsListObj1 = JSON.stringify(environmentSettingsListObj1);
      //    console.log('222',environmentSettingsListObj1);
      //     localStorage.setItem('environment_settings_list1', environmentSettingsListObj1);
      //     this.environmentSettingListObj1 = JSON.parse(localStorage.getItem('environment_settings_list1'));
      //  console.log('!!!', this.environmentSettingListObj);
        
          // if(this.environmentSettingListObj1["withdrawal_min_value1"].currencyId=='1'){

          // }


          }
         
        }
      }, reason => {
        //   wip(0);
        this.data.logout();
        if (reason.error.error == 'invalid_token') {

          this.data.alert('Session Timeout. Login Again', 'warning');
        } else this.data.alert('Could Not Connect To Server', 'danger');
      });
  }
  accountNo;
  beneficiaryName;
  bankName;
  routingNo;
  bankAddress;
  lockOutgoingTransactionStatus;
  withdrawOtp;

  getBankDetails() {
    var bankDetailsObj = {};
    bankDetailsObj["userId"] = localStorage.getItem("user_id");
    var jsonString = JSON.stringify(bankDetailsObj);
    // wip(1);
    this.http
      .post<any>(
        this.data.WEBSERVICE + "/user/GetUserBankDetails",
        jsonString,
        {
          headers: {
            "Content-Type": "application/json",
            authorization: "BEARER " + localStorage.getItem("access_token")
          }
        }
      )
      .subscribe(
        response => {
          // wip(0);
          var result = response;
          if (result.error.error_data != "0") {
            this.data.alert(result.error.error_msg, "danger");
          } else {
            this.accountNo = result.bankDetails.account_no;
            this.beneficiaryName = result.bankDetails.benificiary_name;
            this.bankName = result.bankDetails.bank_name;
            this.routingNo = result.bankDetails.routing_no;
            this.bankAddress = result.bankDetails.address;
          }
        },
        function(reason) {
          // wip(0);
          if (reason.data.error == "invalid_token") {
            this.data.logout();
          } else {
            this.data.logout();
            this.data.alert("Could Not Connect To Server", "danger");
          }
        }
      );
  }
  getUserbalance(){
    this.usdbalance = localStorage.getItem('usdbalance');
  }
  withdraw_fund_amount;
  currencyID=1;
  withdrawFund() {
    //   console.log(this.withdraw_fund_amount);
    if (
      
      this.main.userBankVerificationStatus()
    ) {
      if (this.withdraw_fund_amount != undefined) {
        if(this.CurrencyCode=='USD'){
        if (
          parseFloat(this.withdraw_fund_amount) >=
            parseFloat(this.minvalue) &&
          parseFloat(this.withdraw_fund_amount) <=
            parseFloat(this.maxvalue)
        ) {
          var withdawObj = {};
          withdawObj["userId"] = localStorage.getItem("user_id");
          withdawObj[
            "currencyId"
          ] = this.currencyId;
          withdawObj["sendAmount"] = this.withdraw_fund_amount.toFixed(2);
          withdawObj["bankName"] = this.bankName;
          withdawObj["ifscCode"] = this.routingNo;
          withdawObj["beneficiaryName"] = this.beneficiaryName;
          if (this.lockOutgoingTransactionStatus == 1) {
            withdawObj["otp"] = this.withdrawOtp;
          }
          var jsonString = JSON.stringify(withdawObj);
          // wip(1);
          this.http
            .post<any>(
              this.data.WEBSERVICE + "/transaction/createWithdrawalOrder",
              jsonString,
              {
                headers: {
                  "Content-Type": "application/json",
                  authorization:
                    "BEARER " + localStorage.getItem("access_token")
                }
              }
            )
            .subscribe(
              response => {
                // wip(0);
                var result = response;
                if (result.error.error_data != "0") {
                  this.data.alert(result.error.error_msg, "dark");
                } else {
                  this.withdraw_fund_amount = "";
               
                  this.data.alert("Withdrawal Done Successfully", "success");
                  // this.usdbalance="";
                  this.modalService.dismissAll();
                  this.main.getUserTransaction();
                  // this.getUserbalance();
                }
              },
              function(reason) {
                // wip(0);
                if (reason.data.error == "invalid_token") {
                  this.logout();
                } else {
                  alert("Could Not Connect To Server");
                }
              }
            );
        } else {
          this.data.alert("Please Provide Proper Amount", "danger");
        }
      }
      else{
        if (
          parseFloat(this.withdraw_fund_amount) >=
            parseFloat(this.minvalueAed) &&
          parseFloat(this.withdraw_fund_amount) <=
            parseFloat(this.maxvalueAed)
        ) {
          var withdawObj = {};
          withdawObj["userId"] = localStorage.getItem("user_id");
          withdawObj[
            "currencyId"
          ] = this.currencyId;
          withdawObj["sendAmount"] = this.withdraw_fund_amount.toFixed(2);
          withdawObj["bankName"] = this.bankName;
          withdawObj["ifscCode"] = this.routingNo;
          withdawObj["beneficiaryName"] = this.beneficiaryName;
          if (this.lockOutgoingTransactionStatus == 1) {
            withdawObj["otp"] = this.withdrawOtp;
          }
          var jsonString = JSON.stringify(withdawObj);
          // wip(1);
          this.http
            .post<any>(
              this.data.WEBSERVICE + "/transaction/createWithdrawalOrder",
              jsonString,
              {
                headers: {
                  "Content-Type": "application/json",
                  authorization:
                    "BEARER " + localStorage.getItem("access_token")
                }
              }
            )
            .subscribe(
              response => {
                // wip(0);
                var result = response;
                if (result.error.error_data != "0") {
                  this.data.alert(result.error.error_msg, "dark");
                } else {
                  this.withdraw_fund_amount = "";
               
                  this.data.alert("Withdrawal Done Successfully", "success");
                  // this.usdbalance="";
                  this.modalService.dismissAll();
                  this.main.getUserTransaction();
                  // this.getUserbalance();
                }
              },
              function(reason) {
                // wip(0);
                if (reason.data.error == "invalid_token") {
                  this.logout();
                } else {
                  alert("Could Not Connect To Server");
                }
              }
            );
        } else {
          this.data.alert("Please Provide Proper Amount", "danger");
        }

      }

      } else {
        this.data.alert("Please Provide Proper Amount", "warning");
      }
    } else {
      $("#wfn").attr("disabled", true);
      this.data.alert("Your Bank Details are being verified", "warning");
    }
  }

  ngDoCheck() {
    this.usdbalance = localStorage.getItem('usdbalance');
  }

  determineLockStatusForWithdraw() {
    var userAppSettingsObj = JSON.parse(
      localStorage.getItem("user_app_settings_list")
    );
    this.lockOutgoingTransactionStatus =
      userAppSettingsObj.lock_outgoing_transactions;
    if (this.lockOutgoingTransactionStatus == 1) {
      //   $('#otpModal').modal('show');
      this.data.alert("You are not eligble to withdraw", "danger");
    } else {
      this.withdrawFund();
    }
  }

  resendOtpForOutgoing() {
    var otpObj = {};
    otpObj["email"] = localStorage.getItem("email");
    var jsonString = JSON.stringify(otpObj);
    this.http
      .post<any>(this.data.WEBSERVICE + "/user/ResendOTP", jsonString, {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .subscribe(
        response => {
          var result = response;
          if (result.error.error_data != "0") {
            this.data.alert(result.error.error_msg, "danger");
          } else {
            $(".send_btn").show();
            $(".get_Otp_btn").hide();
          }
        },
        reason => {
          this.data.logout();
          //   wip(0);
          this.data.alert("Could Not Connect To Server", "danger");
        }
      );
  }

  getWithdrawModal(md,elem,bal,cId){
    this.currencyId=cId;
     this.CurrencyCode = elem;
     this.currencyValue = bal;
    this.modalService.open(md, {
      centered: true
    });
  }

}
