import {
  Injectable
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  CoreDataService
} from './core-data.service';
// import {
//   TradesComponent
// } from "./trades/trades.component";
import * as $ from 'jquery';
import {
  Router
} from '@angular/router';
//import { OrderBookComponent } from './order-book/order-book.component';

@Injectable({
  providedIn: 'root'
})
export class BodyService {
  cryptoCurrency: any;
  lockOutgoingTransactionStatus: any;
  paybito_phone: string;
  paybito_amount: string;
  paybito_otp: string;
  other_address: string;
  other_amount: string;
  other_otp: string;
  recievingAddress: any = 0;

  selelectedBuyingAssetBalance: any;
  selelectedSellingAssetBalance: any;
  rcvCode: string;
  balance_list: {
    currency: string;
    balance: number;
    send: number;
    receive: number;
  }[];
  CurrencyBalance: {
    currency: string;
    balance: number;
    send: number;
    receive: number;
  }[];
  loader: boolean;
  pgn: any = [];
  trigxBalance: number;

  constructor(private http: HttpClient, private data: CoreDataService, private route: Router) {
    //this.orderbk.tradePageSetup();
  }
  //get user transaction
  totalFiatBalance;
  triggerslink: any = 'https://xchain.io/tx/';
  fiatBalanceLabel;
  btcBalance;
  bchBalance;
  hcxBalance;
  iecBalance;
  ethBalance;
  ltcBalance;
  buyPrice;
  btcBalanceInUsd;
  usdBalance;
  sellPrice;
  buyPriceText;
  sellPriceText;
  fiatBalance;
  fiatBalanceText: string;
  selectedCryptoCurrency;
  selectedCryptoCurrencyBalance;
  triggersBalance;
  bchBalanceInUsd;
  hcxBalanceInUsd;
  iecBalanceInUsd;
  ethBalanceInUsd;
  btcBought;
  btcSold;
  bchBought;
  bchSold;
  hcxBought;
  hcxSold;
  iecBought;
  iecSold;
  ethBought;
  //new etc
  etcBought;
  etcSold;
  etcBalance;
  etcBalanceInUsd;
  //diam
  diamBought;
  diamSold;
  diamBalance;
  diamBalanceInUsd;
  //new bsv balance
  bsvBought;
  bsvSold;
  bsvBalance;
  bsvBalanceInUsd;
  //new currency

  ethSold;
  ltcBought;
  ltcSold;
  noOfItemPerPage = '20';
  timeSpan = 'all';
  balencelist;
  ethBAlance;
  bccBalance;
  xrpBalance;
  aedbalance:any;
  CurrencyId:any;
  usdbalance:any;
 public alertmessage:any;

  getUserTransaction() {
   var userTransObj = {};
    userTransObj['customerId'] = localStorage.getItem('user_id');
    var jsonString = JSON.stringify(userTransObj);

    this.http.post<any>(this.data.WEBSERVICE + '/transaction/getUserBalance', jsonString, {
      headers: {
        'Content-Type': 'application/json',
        'authorization': 'BEARER ' + localStorage.getItem('access_token')
      }
    })
      .subscribe(response => {
        var result = response;
        // console.log('++++++++++++','result');
        if (result.error.error_data != '0') {
          this.data.alert('Cannot fetch user balance', 'danger');
        } else {
          this.balencelist = result.userBalanceList;
          if(this.balencelist!=null){
            for(var i=0;i<this.balencelist.length;i++){
              if(this.balencelist[i].currencyCode=="USD"){
                this.usdbalance=this.balencelist[i].closingBalance;
                this.CurrencyId = this.balencelist[i].currencyId;
              }
              if(this.balencelist[i].currencyCode=="AED"){
                this.aedbalance=this.balencelist[i].closingBalance;
                this.CurrencyId = this.balencelist[i].currencyId;
              }
            }
          }
     

        
        }
           
   
     
        this.selectedCryptoCurrency = localStorage.getItem('selected_currency');
           // console.log('+++++++++++++++++', this.balencelist);
            for (var i = 0; i <= this.balencelist.length-1; i++) {

           
            }
         //   console.log('********',this.usdBalance, this.btcBalance,this.ethBAlance,this.bccBalance,this.diamBalance,this.ltcBalance,this.hcxBalance,this.etcBalance,this.bsvBalance,this.xrpBalance);

      }, reason => {
        //   wip(0);
        this.data.logout();
        if (reason.error.error == 'invalid_token') {

          this.data.alert('Session Timeout. Login Again', 'warning');
        } else this.data.alert('Could Not Connect To Server', 'danger');
      });

  }

  environmentSettingListObj;
  selectedCurrency;
  buyDisclaimer;
  buyTxnDisclaimer;
  sellDisclaimer;
  sellTxnDisclaimer;
  sendDisclaimer;
  sendMiningDisclaimer;
 public indentificationStatus:any;
 public indentificationStatustwo:any;
  bankDetailStatus;

  getDashBoardInfo() {
    var infoObj = {};
    infoObj['userId'] = localStorage.getItem('user_id');
    var jsonString = JSON.stringify(infoObj);
    this.http.post<any>(this.data.WEBSERVICE + '/user/GetUserAppSettings', jsonString, {
      headers: {
        'Content-Type': 'application/json',
        'authorization': 'BEARER ' + localStorage.getItem('access_token'),
      }
    })
      .subscribe(response => {
        var result = response;
      //  localStorage.setItem("currencyId",result.settingsList[i].currencyId);
     //   console.log('+++++++++++appsettings',response);
        if (result.error.error_data != '0') {
          //    alert(result.error.error_msg);
        } else {
          var storeDashboardInfo = JSON.stringify(result);
          var environmentSettingsListObj: any = {};
          localStorage.setItem('user_app_settings_list', JSON.stringify(result.userAppSettingsResult));
          for (var i = 0; i < result.settingsList.length; i++) {
           // alert(result.settingsList[i].currencyId);
            environmentSettingsListObj['' + result.settingsList[i].name + result.settingsList[i].currencyId + ''] = result.settingsList[i];
          }
          environmentSettingsListObj = JSON.stringify(environmentSettingsListObj);
          localStorage.setItem('environment_settings_list', environmentSettingsListObj);
          localStorage.setItem('user_app_settings_list_tire', JSON.stringify(result.tierWiseTransactionSettingsList));
         ////showing disclaimer for sell
          this.environmentSettingListObj = JSON.parse(localStorage.getItem('environment_settings_list'));
        
          if (result.userAppSettingsResult.user_docs_status == '') {
            this.indentificationStatus = 'You are in Tier 1 , please submit documents for Tier 2 .';
                 
            this.data.alertm('PLEASE SUBMIT DOCUMENTS, ENSURE THAT ALL DOCUMENTS ARE IN JPG OR JPEG FORMAT. PNG, GIF, and other formats are not permitted.', 'info');
          }
          if (result.userAppSettingsResult.user_docs_status == '1') {

            this.indentificationStatus = '  Tier 2 Identity verification documents verified.';
          //  this.data.alertm('Identity verification documents verified.','info');
          }
          if (result.userAppSettingsResult.user_docs_status == '0') {
            this.indentificationStatus = ' Tier 2 Identity verification documents submitted .';
           // this.data.alertm('Identity verification documents submitted for Verification','info');
          }
          if (result.userAppSettingsResult.user_docs_status == '2') {
            this.indentificationStatus = ' Tier 2 Identity verification documents submitted .';
            // this.data.alertm('Identity verifica Identity verification documents declined, please submit again .';
           // this.data.alertm('Identity verification documents declined, please submit again','info');
          }
          if (result.userAppSettingsResult.userTierDocsStatus == '') {
            this.indentificationStatustwo = ' You are already verified for Tier 2 .';
           // this.data.alertm('Identity verification documents declined, please submit again','info');
          }
          if (result.userAppSettingsResult.userTierDocsStatus == '0') {
            this.indentificationStatustwo = ' Tier 3 Identity verification documents submitted . ';
        
          }
          if (result.userAppSettingsResult.userTierDocsStatus == '1') {
            this.indentificationStatustwo = '  Tier 3 Identity verification documents verified .';
           // this.data.alertm('Identity verification documents declined, please submit again','info');
          }
          if (result.userAppSettingsResult.userTierDocsStatus == '2') {
            this.indentificationStatustwo = ' Tier 3 Identity verification documents declined, please submit again';
           // this.data.alertm('Identity verification documents declined, please submit again','info');
          }
          if (result.userAppSettingsResult.bank_details_status == '') {
            this.bankDetailStatus = 'Bank details not submitted';
          //  this.data.alertm('Bank details not submitted','info');
          }
          if (result.userAppSettingsResult.bank_details_status == '0') {
            this.bankDetailStatus = 'Bank details  submitted for Verification';
           // this.data.alertm('Bank details  submitted for Verification','info');
          }
          if (result.userAppSettingsResult.bank_details_status == '2') {
            this.bankDetailStatus = ' Bank details verified';
           // this.data.alertm('Bank details verified','info');
            }
          if (result.userAppSettingsResult.bank_details_status == '3') {
            this.bankDetailStatus = ' Bank documents declined, please submit again';
           // this.data.alertm('Bank documents declined, please submit again','info');

          }
          if (
            localStorage.getItem('check_id_verification_status') &&
            result.userAppSettingsResult.user_docs_status == ''
          ) {
            // this.nav.alert('Please submit Identity verification documents to access all Paybito features','danger');
          }
          localStorage.setItem('check_id_verification_status', 'false');
        }
      }, reason => {
        //   wip(0);
        this.data.logout();
        if (reason.error.error == 'invalid_token') {

          this.data.alert('Session Timeout. Login Again', 'warning');
        } else this.data.alert('Could Not Connect To Server', 'danger');
      });
  }
 
  verificationTitle;
  verificationText

  userDocVerificationStatus() {
    var userAppSettingsObj = JSON.parse(localStorage.getItem('user_app_settings_list'));
    var userDocStatus = userAppSettingsObj.user_docs_status;
    if (userDocStatus == '') {
      this.verificationTitle = 'Submit ID Verification';
      this.verificationText = 'Please submit Identity verification documents to access all Bitrump features';
      this.data.alert(this.verificationText, 'danger');
      this.route.navigateByUrl('/identity-verification');
      return false;
    } else if (userDocStatus == '2') {
      //todo: show Modal;
      this.verificationTitle = 'Submit ID Verification';
      this.verificationText = 'Your Identity verification documents has been declined in the verification process. Please submit again.';
      this.data.alert(this.verificationText, 'warning');
      this.route.navigateByUrl('/identity-verification');
      return false;
    } else if (userDocStatus == '0') {
      //todo: show Modal;
      this.verificationTitle = 'Document Verification Pending';
      this.verificationText = 'Your Id proof documents have not yet been verified by us. You will have restricted access to the features of the app until they are approved.';
      this.data.alert(this.verificationText, 'info');
      return false;
    } else {
      return true;
    }
  }

  userBankVerificationStatus() {
    
    var userAppSettingsObj = JSON.parse(localStorage.getItem('user_app_settings_list'));
    var userBankStatus = userAppSettingsObj.bank_details_status;
    if (userBankStatus == '') {
      this.verificationTitle = 'Submit Payment Method';
      this.verificationText = 'Your Payment details are not yet submitted. Please submit your Payment Details to proceed';

      return false;
    } else if (userBankStatus == '0') {
      //todo: show Modal;
      this.verificationTitle = 'Payment Being Verified';
      this.verificationText = 'Your Payment details are being verified. This step will be available after verification.';
      return false;
    } else if (userBankStatus == '3') {
      this.verificationTitle = 'Submit Payment Method';
      this.verificationText = 'Your Payment details have been disapproved. Kindly submit the details again to proceed.';
      return false;
    } else {
      return true;
    }
  }
  paginationBtn;
  pagination(totalCount, noOfItemPerPage, functionName) {
    var paginationButtonCount = parseInt(totalCount) / parseInt(noOfItemPerPage);
    this.paginationBtn = '';
    // $('.pagination_div').html('');
    if (Math.ceil(paginationButtonCount) >= 1) {
      for (var i = 1; i <= paginationButtonCount + 1; i++) {
        this.paginationBtn += '<button type="button" class="btn btn-dark font-xs filter-button" onclick="angular.element(this).scope().' + functionName + '(' + i + ')" >' + i + '</button>';
      }
    } else {
      this.paginationBtn += '';
    }

    // $('.pagination_div').html(this.paginationBtn);
  }

  totalCount;
  historyDetails;
  historyTableTr;
  selectedCurrencyText;
  status:any;

  transactionHistory(pageNo) {
  
    this.historyTableTr = `<tr>
    <td colspan="5" class="text-center py-3">
    <img src="./assets/svg-loaders/puff.svg" width="50" alt="">
    </td>
  </tr>`;
    $('.historyTableBody').html(this.historyTableTr);
    var historyObj = {};
    historyObj['pageNo'] = pageNo;
    historyObj['noOfItemsPerPage']=20;
    historyObj['userId'] = localStorage.getItem('user_id');
    historyObj['timeSpan'] = this.timeSpan;
    historyObj['transactionType']='all';
    var jsonString = JSON.stringify(historyObj);
    //   wip(1);
    this.http.post<any>(this.data.WEBSERVICE + '/transaction/getUserAllTransaction', jsonString, {
      headers: {
        'Content-Type': 'application/json',
        'authorization': 'BEARER ' + localStorage.getItem('access_token'),
      }
    })
      .subscribe(response => {
        //   wip(0);
        this.historyTableTr = '';
        var result = response;

        if (result.error.error_data != '0') {
          this.data.alert(result.error.error_msg, 'danger');
        } else {
          this.historyDetails = result.userTransactionsResult;
       //   console.log('++++++++++dhriti',this.historyDetails);
          this.totalCount = result.totalCount;
          if (this.historyDetails != null) {
            for (var i = 0; i < this.historyDetails.length; i++) {
              var timestamp = this.historyDetails[i].transactionTimestamp;
              var dt = new Date(this.historyDetails[i].transactionTimestamp);
              var timestampArr = timestamp.split('.');
              timestamp = this.data.readable_timestamp(timestampArr[0]);
              var action = this.historyDetails[i].action;
              this.selectedCurrency = localStorage.getItem('selected_currency').toUpperCase();
              this.selectedCurrencyText = this.selectedCurrency;
              // if (this.historyDetails[i].baseCurrency != 'usd') {
              //   var baseCurrency = this.historyDetails[i].baseCurrency;
              // } else {
              //   var baseCurrency: any = 'fiat';
              // }
              // if (this.historyDetails[i].currency != 'usd') {
              //   var counterCurrency = this.historyDetails[i].currency;
              // } else {
              //   var counterCurrency: any = 'fiat';
              // }
              if (action == 'Buy') {
                var amount =  '<span class="text-white">'+ this.historyDetails[i]['tradeAssetAmount'] +'</span> ';
                // if (this.historyDetails[i]['debitAmount'] != 0
                //   && this.historyDetails[i]['creditAmount'] != 0) {
                //   var amount =  '<span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' ' + this.historyDetails[i]['creditAmount'] + ' Cr.</span>';
                // } else {
                //   if (this.historyDetails[i]['debitAmount'] != 0) {
                //     var amount = '<span class="text-white">' + (this.historyDetails[i].baseCurrency).toUpperCase() + ' ' + this.historyDetails[i]['debitAmount'] + ' Dr. </span> ';
                //   }
                //   if (this.historyDetails[i]['creditAmount'] != 0) {
                //     var amount = '<span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' ' + this.historyDetails[i]['creditAmount'] + ' Cr. </span> ';
                //   }
                // }
              }
              if (action == 'Buyoffer') {
                var amount =  '<span class="text-white">'+ this.historyDetails[i]['tradeAssetAmount'] +'</span> ';
                // var amount = '<span class="text-white">' + (this.historyDetails[i].baseCurrency).toUpperCase() + ' ' + this.historyDetails[i]['debitAmount'] + ' Dr. </span> ';
              //  var amount =  '<span class="text-white">'+ this.historyDetails[i]['tradeAssetAmount'] +'</span> ';
              }
              if (action == 'Sell') {
                var amount =  '<span class="text-white">'+ this.historyDetails[i]['tradeAssetAmount'] +'</span> ';
                // if (this.historyDetails[i]['creditAmount'] != 0 && this.historyDetails[i]['debitAmount'] != 0) {
                //   // var amount = '<span class="text-white">' + (this.historyDetails[i].baseCurrency).toUpperCase() + ' ' + this.historyDetails[i]['creditAmount'] + ' Dr. </span>';
                //   var amount = '<span class="text-white">' + (this.historyDetails[i].baseCurrency).toUpperCase() + ' ' + this.historyDetails[i]['creditAmount'] + ' Cr. </span>';
                // } else {
                //   if (this.historyDetails[i]['creditAmount'] != 0) {
                //     var amount = '<span class="text-white">' + (this.historyDetails[i].baseCurrency).toUpperCase() + ' ' + this.historyDetails[i]['creditAmount'] + ' Cr. </span> ';
                //   }
                //   if (this.historyDetails[i]['debitAmount'] != 0) {
                //     var amount = '<span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' ' + this.historyDetails[i]['debitAmount'] + ' Dr. </span> ';
                //   }
                // }
              }

              //Buydel
              if (action == 'Buydel') {
                var amount =  '<span class="text-white">'+ this.historyDetails[i]['tradeAssetAmount'] +'</span> ';
                // if (this.historyDetails[i].baseCurrency == 'usd' && this.historyDetails[i]['creditAmount'] == 0) {
                //   var amount = '<span class="text-white">' + (this.historyDetails[i].baseCurrency).toUpperCase() + ' '
                //     + this.historyDetails[i]['debitAmount'] + ' Dr. </span> ';
                // }
                // else if (this.historyDetails[i].baseCurrency == 'usd' && this.historyDetails[i]['debitAmount'] == 0) {
                //   var amount = '<span class="text-white">' + (this.historyDetails[i].baseCurrency).toUpperCase() + ' '
                //     + this.historyDetails[i]['creditAmount'] + ' Cr. </span> ';
                // }
                // else if (this.historyDetails[i].baseCurrency != 'usd' && this.historyDetails[i]['debitAmount'] == 0) {
                //   var amount = '<span class="text-white">' + (this.historyDetails[i].baseCurrency).toUpperCase() + ' '
                //     + this.historyDetails[i]['debitAmount'] + ' Dr. </span> ';
                // }
                // else {
                //   var amount = '<span class="text-white">' + (this.historyDetails[i].baseCurrency).toUpperCase() + ' '
                //     + this.historyDetails[i]['creditAmount'] + ' Cr. </span> ';
                // }
              }
              if (action == 'Selldel') {
                var amount =  '<span class="text-white">'+ this.historyDetails[i]['tradeAssetAmount'] +'</span> ';
                // if (this.historyDetails[i]['creditAmount'] == 0) {
                //   var amount = '<span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' '
                //     + this.historyDetails[i]['debitAmount'] + ' Dr. </span> ';
                // }
                // else {
                //   var amount = '<span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' '
                //     + this.historyDetails[i]['creditAmount'] + ' Cr. </span> ';
                // }
              }

              //end Buydel
              //RollBack Code
              if (action == 'Rollback' || action == 'rollback' || action == 'RollBack') {
                var amount =  '<span class="text-white">'+ this.historyDetails[i]['tradeAssetAmount'] +'</span> ';
              //   if (this.historyDetails[i].baseCurrency != 'usd' && this.historyDetails[i].currency != 'usd') {
              //     if ((this.historyDetails[i]['creditAmount']) != 0) {
              //       if (this.historyDetails[i].baseCurrency == '' || this.historyDetails[i].baseCurrency == '-') {
              //         var amount = '<span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' '
              //           + this.historyDetails[i]['creditAmount'] + ' Cr. </span> ';
              //       }
              //     }
              // }
            }
              //Rollback Code Ends
              //BuyOffer Modify
              if (action == 'Buymodify') {
                var amount =  '<span class="text-white">'+ this.historyDetails[i]['tradeAssetAmount'] +'</span> ';
                // if (this.historyDetails[i].baseCurrency == 'usdt' && this.historyDetails[i]['creditAmount'] == null) {
                //   var amount = '<span class="text-white">' + (this.historyDetails[i].baseCurrency).toUpperCase() + ' '
                //     + this.historyDetails[i]['debitAmount'] + ' Dr. </span> ';
                // }
                // else if (this.historyDetails[i].baseCurrency == 'usdt' && this.historyDetails[i]['debitAmount'] == null) {
                //   var amount = '<span class="text-white">' + (this.historyDetails[i].baseCurrency).toUpperCase() + ' '
                //     + this.historyDetails[i]['creditAmount'] + ' Cr. </span> ';
                // }
                // else if (this.historyDetails[i].baseCurrency != 'usdt' && this.historyDetails[i]['creditAmount'] == null) {
                //   var amount = '<span class="text-white">' + (this.historyDetails[i].baseCurrency).toUpperCase() + ' '
                //     + this.historyDetails[i]['debitAmount'] + ' Dr. </span> ';
                // }
                // else {
                //   var amount = '<span class="text-white">' + (this.historyDetails[i].baseCurrency).toUpperCase() + ' '
                //     + this.historyDetails[i]['creditAmount'] + ' Cr. </span> ';
                // }
              }

              //BuyOffer Modify End

              //SellOffer Modify
              if (action == 'Sellmodify') {
                var amount =  '<span class="text-white">'+ this.historyDetails[i]['tradeAssetAmount'] +'</span> ';
                // if (this.historyDetails[i].currency != 'usdt' && this.historyDetails[i]['creditAmount'] == null) {
                //   var amount = '<span class="text-white">' + (this.historyDetails[i].currency) + ' '
                //     + this.historyDetails[i]['debitAmount'] + ' Dr. </span> ';
                // }
                // else if(this.historyDetails[i].currency != 'usdt' && this.historyDetails[i]['debitAmount'] == null){
                //   var amount = '<span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' '
                //     + this.historyDetails[i]['creditAmount'] + ' Cr. </span> ';
                // }
                // else {
                //   var amount = '<span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' '
                //     + this.historyDetails[i]['creditAmount'] + ' Cr. </span> ';
                // }
               
              }
              //SellOffer Modify End
              if (action == 'Selloffer') {
                //var amount = '<span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' ' + this.historyDetails[i]['debitAmount'] + ' Dr. </span> ';
                var amount =  '<span class="text-white">'+ this.historyDetails[i]['tradeAssetAmount'] +'</span> ';
              }
               var hrefForTxn: any;
              if (action == 'Send' || action == 'Sent') {
                if (this.historyDetails[i]['debitAmount'] != 0) {
                  var amount =  '<span class="text-white">'+ this.historyDetails[i]['tradeAssetAmount'] +'</span> ';
                  if (this.historyDetails[i].currencyTxnid != null && this.historyDetails[i].currencyTxnid != 'null') {
                    if ((this.historyDetails[i].currencyTxnid).length >= 10) {
                      hrefForTxn = '<a target="_blank" href="https://blockexplorer.com/tx/' + this.historyDetails[i].currencyTxnid + '">(Check Transaction Block)</a>';
                    }
                  }
                }

              }
              if (action == 'Received' || action == 'Receive') {
                if (this.historyDetails[i]['creditAmount'] != 0) {
                  var amount =  '<span class="text-white">'+ this.historyDetails[i]['tradeAssetAmount'] +'</span> ';
                  if (this.historyDetails[i].currencyTxnid != null && this.historyDetails[i].currencyTxnid != 'null') {
                    if ((this.historyDetails[i].currencyTxnid).length >= 10) {
                      hrefForTxn = '<a target="_blank" href="https://blockexplorer.com/tx/' + this.historyDetails[i].currencyTxnid + '">(Check Transaction Block)</a>';
                    }
                  }
                }

              }
              if (action == 'Load') {
                if(this.historyDetails[i].currency =='USD' || this.historyDetails[i].baseCurrency =='USD'){
                  var amount = ' <span class="text-white">' + this.data.CURRENCYICON + ' ' + this.historyDetails[i].creditAmount + ' Dr.</span>';
                }
                else{
                  var amount = ' <span class="text-white">' + 'د.إ' + ' ' + this.historyDetails[i].creditAmount + ' Dr.</span>';
                }
              }
              if (action == 'Withdraw') {
                if(this.historyDetails[i].currency =='USD' || this.historyDetails[i].baseCurrency =='USD'){
                  var amount = ' <span class="text-white">' + this.data.CURRENCYICON + ' ' + this.historyDetails[i].debitAmount + ' Dr.</span>';
                }
                else{
                  var amount = ' <span class="text-white">' + 'د.إ' + ' ' + this.historyDetails[i].debitAmount + ' Dr.</span>';
                }
              }
              if (action == 'Decline') {
                var amount = '<span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' ' + this.historyDetails[i]['creditAmount'] + ' Cr. </span>';
              }
              if (action == 'Send Decline') {
                var amount = '<span class="text-white">' + (this.historyDetails[i].currency).toUpperCase() + ' ' + this.historyDetails[i]['debitAmount'] + ' Dr. </span>';
              }
              if (this.historyDetails[i].status == '0') {
                status = 'Pending';
                var statusClass = 'text-orange';
              } else if (this.historyDetails[i].status == '1') {
                status = 'Confirmed';
                var statusClass = 'text-green';
              } else{
                status = this.historyDetails[i].action;
                var statusClass = 'text-red';
              }
              // if (this.historyDetails[i].status == 'cancel') {
              //   var status = 'Cancelled';
              //   var statusClass = 'text-red';
              // }
              if (this.historyDetails[i].orderid != null) {
                var transactionId = this.historyDetails[i].orderid;
              } else {
                var transactionId = this.historyDetails[i].transactionId;
              }

              if (action == 'Buyoffer') {
                var action: any = 'Buy Offer';
              }
              if (action == 'Selloffer') {
                var action: any = 'Sell Offer';
              }

              this.historyTableTr += '<tr>';
              this.historyTableTr += '<td>' + timestamp + '</td>';
              this.historyTableTr += '<td>' + transactionId + '</td>';
              this.historyTableTr += '<td class="text-white">' + action + '</td>';
              this.historyTableTr += '<td>' + amount + '</td>';
              this.historyTableTr += '<td class="' + statusClass + '">' + status + '</td>';
              this.historyTableTr += '</tr>';
            }
            //   this.pagination(this.totalCount,this.noOfItemPerPage,'transactionHistory');
          } else {
            this.historyTableTr += '<tr><td colspan="5" class="text-center">No Data Exist</td></tr>';
          }
          $('.historyTableBody').html(this.historyTableTr);
          //   console.log(this.historyTableTr);

          //all
          this.pgn = [];
          for (i = 1; i <= Math.ceil(this.totalCount / 20); i++) {
            this.pgn.push(i);
          }
          // console.log(this.pgn);

        }
      }, reason => {
        //   wip(0);
        this.data.logout();
        if (reason.error.error == 'invalid_token') {

          this.data.alert('Session Timeout. Login Again', 'warning');
        } else this.data.alert('Could Not Connect To Server', 'danger');
      });

  }

  fullName;
  editName;
  userName;
  address;
  country;
  email;
  editEmail;
  phone;
  profilePic;
  joinDate;

  getUserDetails() {

    this.loader = true;
    var userObj = {};
    userObj['userId'] = localStorage.getItem('user_id');
    var jsonString = JSON.stringify(userObj);
    // wip(1);
    this.http.post<any>(this.data.WEBSERVICE + '/user/GetUserDetails', jsonString, {
      headers: {
        'Content-Type': 'application/json',
        'authorization': 'BEARER ' + localStorage.getItem('access_token'),
      }
    })
      .subscribe(response => {
        //   wip(0);
        var result = response;
       // console.log('result+++++++',result);

        if (result.error.error_data != '0') {

        } else {
          this.loader = false;
          this.fullName = (result.userListResult[0].fullName);
          this.editName = result.userListResult[0].fullName;
          this.userName = result.userListResult[0].fullName;
          this.address = result.userListResult[0].address;
          this.country = result.userListResult[0].country;
          this.email = result.userListResult[0].email;
          this.editEmail = result.userListResult[0].email;
          this.phone = result.userListResult[0].phone;
          if (localStorage.getItem('profile_pic') != '') {
            this.profilePic = this.data.WEBSERVICE + '/user/' + localStorage.getItem('user_id') + '/file/' + result.userListResult[0].profilePic + '?access_token=' + localStorage.getItem('access_token');
          } else {
            this.profilePic = './assets/img/default.png';
          }
          this.joinDate = this.data.readable_timestamp(result.userListResult[0].created);
          this.CurrencyBalance=result.userBalanceList;

          // if(result.userBalanceList !=null){
          //   for(var i=0;i<result.userBalanceList.length;i++){
          //     this.currencyId=result.userBalanceList.currencyId;
          //   //  this. =currencyCode;
          //   //    =closingBalance;
          //   //    =sendAccess;
          //   //    =receiveAccess;
          //   //    =totalBuy;
          //   //   = totalSell;

          //    }

          // }

        }

      }, reason => {
        //   wip(0);
        this.data.logout();
        if (reason.error.error == 'invalid_token') {
          this.data.alert('Session Timeout. Login Again', 'warning');
        } else this.data.alert('Could Not Connect To Server', 'danger');
      });
  }

  getCurrencyForSend(elem) {
    console.log('elem:' + elem);
    this.cryptoCurrency = elem;
    this.selectedCurrency = elem;
    if (this.userDocVerificationStatus() == true) {
      var userAppSettingsObj = JSON.parse(localStorage.getItem('user_app_settings_list'));
      this.lockOutgoingTransactionStatus = userAppSettingsObj.lock_outgoing_transactions;
      if (this.lockOutgoingTransactionStatus == 1) {
        $('.sendOtpSection').show();
        // $('.get_Otp_btn').show();
        $('.send_btn').show();

      } else {
        $('.sendOtpSection').hide();
        // $('.get_Otp_btn').hide();
        $('.send_btn').show();
      }

      this.paybito_phone = '';
      this.paybito_amount = '';
      this.paybito_otp = '';
      this.other_address = '';
      this.other_amount = '';
      this.other_otp = '';

      $('#sendModal').modal('show');
      this.environmentSettingListObj = JSON.parse(localStorage.getItem('environment_settings_list'));
      this.sendDisclaimer = this.environmentSettingListObj['send_other_min_value'][this.cryptoCurrency + '_description'];
      this.sendMiningDisclaimer = this.environmentSettingListObj['send_other_m_charges'][this.cryptoCurrency + '_description'];
    }

  }
  getCurrencyForRecieve(currency) {
    // this.modalService.open(elem, { centered: true });
   // console.log('elem:' + currency);
    this.cryptoCurrency = currency;
    $('.receive_address_label, .receive_address, .recieve_qr_code').hide();
    $('.generate_address_btn').hide();
    $('#qr_code').html('');
    if (this.userDocVerificationStatus() == true) {
      this.generateAddress();
    }

  }

  generateAddress() {
    var rcvObj = {};
    rcvObj['customerID'] = localStorage.getItem('user_id');
    rcvObj['crypto_currency'] = this.cryptoCurrency;
    var jsonString = JSON.stringify(rcvObj);
    // wip(1);
    if (this.cryptoCurrency != 'trigger') {
      this.http.post<any>(this.data.WEBSERVICE + '/userTransaction/ReceiveBTC', jsonString, {
        headers: {
          'Content-Type': 'application/json',
          'authorization': 'BEARER ' + localStorage.getItem('access_token'),
        }
      })
        .subscribe(response => {
          // wip(0);
          var result = response;
          if (result.error.error_data != '0') {
            this.data.alert(result.error.error_msg, 'danger');
          } else {
            this.rcvCode = result.customerkeysResult.fromadd;
          }

        });
    } else {
      this.http.post<any>(this.data.WEBSERVICE + '/userTransaction/GetCounterPartyNewAddress', jsonString, {
        headers: {
          'Content-Type': 'application/json',
          'authorization': 'BEARER ' + localStorage.getItem('access_token'),
        }
      })
        .subscribe(response => {
          // wip(0);
          var result = response;
          if (result.error.error_data != '0') {
            this.data.alert(result.error.error_msg, 'danger');
          } else {
            this.rcvCode = result.customerkeysResult.fromadd;
          }

        });

    }

  }

  sessionExpiredLogout() {
    localStorage.clear();
    // window.location="login.html?reason=Session expired, please login again.";.
    this.route.navigateByUrl('/login');
  }

  setInterval() {

    var userTransObj = {};
    userTransObj['customerId'] = localStorage.getItem('user_id');
    // userTransObj['crypto_currency'] = localStorage.getItem('selected_currency');
    var jsonString = JSON.stringify(userTransObj);
    // wip(1);
    this.http.post<any>(this.data.WEBSERVICE + '/userTransaction/GetUserBalance', jsonString, {
      headers: {
        'Content-Type': 'application/json',
        'authorization': 'BEARER ' + localStorage.getItem('access_token'),
      }
    })
      .subscribe(response => {
        // wip(0);
        var result = response;

        if (result.error.error_data != '0') {
          if (result.error.error_msg == '' || result.error.error_msg == null) {

          } else {
            this.data.alert(result.error.error_msg, 'danger');
          }

          if (result.error.error_msg == 'invalid_token') {
            this.sessionExpiredLogout();
          }
        } else {
          this.totalFiatBalance = this.data.CURRENCYICON + (result.userBalanceResult.fiat_balance).toFixed(2);
          this.fiatBalanceLabel = 'Total ' + this.data.CURRENCYNAME + ' Balance';
          if (result.userBalanceResult.btc_balance == 'null' || result.userBalanceResult.btc_balance == null) {
            this.btcBalance = '0'
          } else {
            this.btcBalance = result.userBalanceResult.btc_balance;
          }
          if (result.userBalanceResult.bch_balance == 'null' || result.userBalanceResult.bch_balance == null) {
            this.bchBalance = '0'
          } else {
            this.bchBalance = result.userBalanceResult.bch_balance;
          }
          if (result.userBalanceResult.hcx_balance == 'null' || result.userBalanceResult.hcx_balance == null) {
            this.hcxBalance = '0'
          } else {

            this.hcxBalance = result.userBalanceResult.hcx_balance;
          }
          if (result.userBalanceResult.iec_balance == 'null' || result.userBalanceResult.iec_balance == null) {
            this.iecBalance = '0'
          } else {
            this.iecBalance = result.userBalanceResult.iec_balance;
          }
          // etc
          if (result.userBalanceResult.etc_balance == 'null' || result.userBalanceResult.etc_balance == null) {
            this.etcBalance = '0'
          } else {
            this.etcBalance = result.userBalanceResult.etc_balance;
          }
          //bsv
          if (result.userBalanceResult.bsv_balance == 'null' || result.userBalanceResult.bsv_balance == null) {
            this.bsvBalance = '0'
          } else {
            this.bsvBalance = result.userBalanceResult.bsv_balance;
          }
          //
          if (result.userBalanceResult.diam_balance == 'null' || result.userBalanceResult.diam_balance == null) {
            this.diamBalance = '0'
          } else {
            this.diamBalance = result.userBalanceResult.diam_balance;
          }
          if (result.userBalanceResult.triggers_balance == 'null' || result.userBalanceResult.triggers_balance == null) {
            this.triggersBalance = '0'
          } else {
            this.triggersBalance = result.userBalanceResult.triggers_balance;
          }
          this.buyPrice = result.userBalanceResult.crypto_buy_price;
          this.btcBalanceInUsd = (parseFloat(this.btcBalance) * parseFloat(this.buyPrice)).toFixed(8);
          this.bchBalanceInUsd = (parseFloat(this.bchBalance) * parseFloat(this.buyPrice)).toFixed(3);
          this.hcxBalanceInUsd = (parseFloat(this.hcxBalance) * parseFloat(this.buyPrice)).toFixed(3);
          this.iecBalanceInUsd = (parseFloat(this.iecBalance) * parseFloat(this.buyPrice)).toFixed(3);
          //new etc
          this.etcBalanceInUsd = (parseFloat(this.etcBalance) * parseFloat(this.buyPrice)).toFixed(3);
          //bsv
          this.bsvBalanceInUsd = (parseFloat(this.bsvBalance) * parseFloat(this.buyPrice)).toFixed(3);
          //diam
          this.diamBalanceInUsd = (parseFloat(this.diamBalance) * parseFloat(this.buyPrice)).toFixed(3);

          // this.buyPrice=(this.buyPrice).toFixed(2);
          this.sellPrice = result.userBalanceResult.crypto_sell_price;
          //this.sellPrice=(this.sellPrice).toFixed(2);
          this.buyPriceText = this.data.CURRENCYICON + ' ' + this.buyPrice;
          this.sellPriceText = this.data.CURRENCYICON + ' ' + this.sellPrice;
          this.fiatBalance = result.userBalanceResult.fiat_balance;
          this.fiatBalanceText = this.data.CURRENCYICON + ' ' + result.userBalanceResult.fiat_balance;
          this.selectedCryptoCurrency = localStorage.getItem('selected_currency');
          if (result['userBalanceResult'][this.selectedCryptoCurrency + '_balance'] == null || result['userBalanceResult'][this.selectedCryptoCurrency + '_balance'] == 'null') {
            this.selectedCryptoCurrencyBalance = '0';
          } else {
            this.selectedCryptoCurrencyBalance = result['userBalanceResult'][this.selectedCryptoCurrency + '_balance'];
          }
          this.btcBought = result.userBalanceResult.bitcoins_bought;
          this.btcSold = result.userBalanceResult.bitcoins_sold;
          this.bchBought = result.userBalanceResult.bitcoinCash_bought;
          this.bchSold = result.userBalanceResult.bitcoinCash_sold;
          this.hcxBought = result.userBalanceResult.hcx_bought;
          this.hcxSold = result.userBalanceResult.hcx_sold;
          this.iecBought = result.userBalanceResult.iec_bought;
          this.iecSold = result.userBalanceResult.iec_sold;
          this.ethSold = result.userBalanceResult.ethereum_sold;
          this.ethBought = result.userBalanceResult.ethereum_bought;
          //new etcBought
          this.etcSold = result.userBalanceResult.etc_sold;
          this.etcBought = result.userBalanceResult.etc_Bought;

          //new bsv
          this.bsvSold = result.userBalanceResult.bsv_sold;
          this.bsvBought = result.userBalanceResult.bsv_Bought;

          this.ltcBought = result.userBalanceResult.ltc_bought;
          this.ltcSold = result.userBalanceResult.ltc_sold;
          //diam
          this.diamBought = result.userBalanceResult.diam_bought;
          this.diamSold = result.userBalanceResult.diam_sold;

        }
      }, reason => {
        //   wip(0);
        if (reason.error.error == 'invalid_token') {
          this.data.logout();
          this.data.alert('Session Timeout. Login Again', 'warning');
        } else this.data.alert('Could Not Connect To Server', 'danger');
      });
  }

}
